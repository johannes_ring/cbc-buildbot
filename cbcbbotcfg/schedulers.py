schedulers = []

from buildbot.schedulers.basic import Scheduler, SingleBranchScheduler
from buildbot.schedulers.forcesched import ForceScheduler, FixedParameter, \
     StringParameter
from buildbot.schedulers.timed import Nightly
from buildbot.changes import filter

from buildbot.schedulers.timed import Periodic
from buildbot.schedulers.trysched import Try_Userpass
from cbcbbotcfg.slaves import slaves
from cbcbbotcfg import builders

import os

def get_pass(name):
    # get the password based on the name
    path = os.path.join(os.path.dirname(__file__), "%s.pass" % name)
    pw = open(path).read().strip()
    return pw

#schedulers.append(Scheduler(name="all",
#                            #branch='trunk',
#                            treeStableTimer=10,
#                            builderNames=[b['name'] for b in builders.builders]))

schedulers.append(ForceScheduler(name="force",
                                 properties=[],
                                 builderNames=[b['name'] for b in builders.builders]))

#schedulers.append(ForceScheduler(name="force",
#    repository=StringParameter(name="repository", label="repository:<br>",
#                               required=True, size=80),
#    branch=StringParameter(name="branch", label="branch:<br>",
#                           required=True, size=80, default="master"),
#    project=StringParameter(name="project", label="project:<br>",
#                            required=True, size=80, default=""),
#    revision=StringParameter(name="revision", label="revision:<br>",
#                             required=False, size=80, default=""),
#    properties=[],
#    builderNames=[b['name'] for b in builders.builders]))

# Nightly schedulers for cbcflow
schedulers.append(Nightly(name="nightly-cbcflow-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('cbcflow-master')],
                          hour=5, minute=30))

# Nightly schedulers for cbcpost
schedulers.append(Nightly(name="nightly-cbcpost-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('cbcpost-master')],
                          hour=5, minute=30))

# Nightly schedulers for OpenTidalFarm
schedulers.append(Nightly(name="nightly-opentidalfarm-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('opentidalfarm-master')],
                          hour=5, minute=30))

# Nightly schedulers for cbcbeat
schedulers.append(Nightly(name="nightly-cbcbeat",
                          branch='',
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('cbcbeat-default')],
                          hour=1, minute=15))

# Nightly schedulers for annualreport
schedulers.append(Nightly(name="nightly-annualreport",
                          branch='',
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('annualreport-default')],
                          hour=0, minute=30))


schedulers.append(
    SingleBranchScheduler(name="cbcflow-master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('cbcflow-master')],
                          change_filter=filter.ChangeFilter(
                              project='cbcflow',
                              branch='master',
                              category='cbcflow.master',
                              )))

schedulers.append(
    SingleBranchScheduler(name="cbcpost-master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('cbcpost-master')],
                          change_filter=filter.ChangeFilter(
                              project='cbcpost',
                              branch='master',
                              category='cbcpost.master',
                              )))

schedulers.append(
    SingleBranchScheduler(name="opentidalfarm-master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('opentidalfarm-master')],
                          change_filter=filter.ChangeFilter(
                              project='opentidalfarm',
                              branch='master',
                              category='opentidalfarm.master',
                              )))

schedulers.append(
    SingleBranchScheduler(name="cbcbeat-default",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('cbcbeat-default')],
                          change_filter=filter.ChangeFilter(
                              project='cbcbeat',
                              branch='',
                              category='cbcbeat.default',
                              )))

schedulers.append(
    SingleBranchScheduler(name="annualreport-default",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('annualreport-default')],
                          change_filter=filter.ChangeFilter(
                              project='annualreport',
                              branch='',
                              category='annualreport.default',
                              )))
