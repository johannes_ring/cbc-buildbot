from buildbot.changes.gitpoller import GitPoller
from buildbot.changes.hgpoller import HgPoller

changes = []

changes.append(GitPoller(repourl='https://bitbucket.org/simula_cbc/cbcflow.git',
                         pollinterval=5*60,
                         branches=['master'],
                         project='cbcflow',
                         category='cbcflow.master'
                         ))

changes.append(GitPoller(repourl='https://bitbucket.org/simula_cbc/cbcpost.git',
                         pollinterval=5*60,
                         branches=['master'],
                         project='cbcpost',
                         category='cbcpost.master'
                         ))

changes.append(GitPoller(repourl='https://github.com/funsim/OpenTidalFarm.git',
                         pollinterval=5*60,
                         branches=['master'],
                         project='opentidalfarm',
                         category='opentidalfarm.master'
                         ))

changes.append(HgPoller(repourl='https://bitbucket.org/meg/cbcbeat/',
                        pollinterval=5*60,
                        project='cbcbeat',
                        category='cbcbeat.default',
                        workdir='hg-myrepo'
                        ))

changes.append(HgPoller(repourl='ssh://hg@bitbucket.org/simulatomat/annualreport-cbc-annualreport2016/',
                        pollinterval=2*60,
                        project='annualreport',
                        category='annualreport.default',
                        workdir='hg-myrepo-report'
                        ))
