status = []

from buildbot.status import html
from buildbot.status.web.authz import Authz
from buildbot.status.web.auth import BasicAuth
from buildbot.status.mail import MailNotifier
from buildbot.status.client import PBListener

from cbcbbotcfg import builders

users = [('dev', 'bbot!')]  # it's not *that* secret..
#authz = Authz(auth=BasicAuth(users), forceBuild='auth')
authz = Authz(
    auth=BasicAuth(users),
    forceBuild=True,
    forceAllBuilds=True,
    cancelPendingBuild=True,
    stopChange=True,
    stopBuild=True)

status.append(html.WebStatus(
    http_port=8010,
    authz=authz,
    order_console_by_time=True,
    projects={"cbcbeat": "https://bitbucket.org/meg/cbcbeat",
              "opentidalfarm": "https://github.com/funsim/OpenTidalFarm",
              "cbcflow": "https://bitbucket.org/simula_cbc/cbcflow",
              "cbcpost": "https://bitbucket.org/simula_cbc/cbcpost",
              },
    ))

status.append(MailNotifier(
    fromaddr="buildbot@cbc.simula.no",
    sendToInterestedUsers=False,
    extraRecipients=['johannr@simula.no',
                     'martinal@simula.no',
                     'oyvinev@simula.no',
                     ],
    mode='problem',
    builders=[b['name'] for b in builders.builders \
              if b['name'].startswith('cbcpost')],
    ))

status.append(MailNotifier(
    fromaddr="buildbot@cbc.simula.no",
    sendToInterestedUsers=False,
    extraRecipients=['johannr@simula.no',
                     'martinal@simula.no',
                     'oyvinev@simula.no',
                     'kent-and@simula.no',
                     ],
    mode='problem',
    builders=[b['name'] for b in builders.builders \
              if b['name'].startswith('cbcflow')],
    ))

status.append(MailNotifier(
    fromaddr="buildbot@cbc.simula.no",
    sendToInterestedUsers=False,
    extraRecipients=['johannr@simula.no',
                     'simon@simula.no',
                     ],
    mode='problem',
    builders=[b['name'] for b in builders.builders \
              if b['name'].startswith('opentidalfarm')],
    ))

status.append(MailNotifier(
    fromaddr="buildbot@cbc.simula.no",
    sendToInterestedUsers=False,
    extraRecipients=['johannr@simula.no',
                     'meg@simula.no',
                     'patrick.farrell@imperial.ac.uk',
                     'simon@simula.no',
                     ],
    mode='problem',
    builders=[b['name'] for b in builders.builders \
              if b['name'].startswith('cbcbeat')],
    ))

status.append(MailNotifier(
    fromaddr="buildbot@cbc.simula.no",
    sendToInterestedUsers=False,
    extraRecipients=['johannr@simula.no',
                     #'tomat@simula.no',
                     ],
    mode='problem',
    builders=[b['name'] for b in builders.builders \
              if b['name'].startswith('annualreport')],
    ))

status.append(PBListener(port=9988))
