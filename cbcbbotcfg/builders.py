import textwrap
import itertools
import os

from buildbot.process import factory
from buildbot.process.factory import BuildFactory
from buildbot.plugins import steps
from buildbot.steps.shell import Configure, Compile, Test, ShellCommand
from buildbot.steps.transfer import FileDownload, FileUpload, DirectoryUpload
from buildbot.steps.python_twisted import Trial
from buildbot.steps.python import PyFlakes
from buildbot.process.factory import Distutils
from buildbot.steps.slave import RemoveDirectory, MakeDirectory
from buildbot.steps.master import MasterShellCommand

from cbcbbotcfg.slaves import slaves, get_slaves, names

# slaves seem to have a hard time fetching from Bitbucket, so retry
# every 5 seconds, 5 times
GIT_RETRY = (5,5)

def update_env(prefix, env, os="linux", pyver="2.7"):
    tmp = env.copy()
    tmp["PATH"] = [prefix + "/bin", "${PATH}"] + tmp.get("PATH", [])
    if os == "osx":
        tmp["DYLD_LIBRARY_PATH"] = [prefix + "/lib",
                                    "${DYLD_LIBRARY_PATH}"] + \
                                     tmp.get("LD_LIBRARY_PATH", [])
    else:
        tmp["LD_LIBRARY_PATH"] = [prefix + "/lib",
                                  "${LD_LIBRARY_PATH}"] + \
                                  tmp.get("LD_LIBRARY_PATH", [])
    tmp["PKG_CONFIG_PATH"] = [prefix + "/lib/pkgconfig",
                              "${PKG_CONFIG_PATH}"] + \
                              tmp.get("PKG_CONFIG_PATH", [])
    tmp["PYTHONPATH"] = [prefix + "/lib/python" + pyver + "/site-packages",
                         "${PYTHONPATH}"] + tmp.get("PYTHONPATH", [])
    return tmp

class PythonBuild(ShellCommand):
    description = ["building"]
    descriptionDone = ["build"]
    name = "build"

class PythonInstall(ShellCommand):
    description = ["installing"]
    descriptionDone = ["install"]
    name = "install"

class MakeInstall(ShellCommand):
    haltOnFailure = 1
    description = ["installing"]
    descriptionDone = ["install"]
    name = "install"

def addpath(envvar, path, env=None):
    """Adds a path to an environment variable."""
    if env is None:
        env = os.environ
    envval = env.get(envvar, path)
    parts = envval.split(os.pathsep)
    parts.insert(0, path)
    # remove duplicate entries:
    i = 1
    while i < len(parts):
        if parts[i] in parts[:i]:
            del parts[i]
        elif envvar == 'PYTHONPATH' and parts[i] == "":
            del parts[i]
        else:
            i += 1
    envval = os.pathsep.join(parts)
    env[envvar] = envval

builders = []

######## BuildFactory Factories

def mk_annualreport_factory(branch='default', install_prefix=None, **kwargs):
    python = kwargs.get("python", os.environ.get("PYTHON", "python"))

    f = factory.BuildFactory()

    # Checkout
    repourl = "ssh://hg@bitbucket.org/simulatomat/annualreport-cbc-annualreport2016/"
    f.addStep(steps.Mercurial(repourl=repourl, mode='full'))

    # Generate annual report
    cmd = "rubber --verbose --force -p -o ps2pdf annualreport2016and17".split()
    f.addStep(ShellCommand(command=cmd,
                           description="generating report",
                           descriptionDone="generate report",
                           name="generate report",
                           haltOnFailure=True, flunkOnFailure=True))

    f.addStep(FileUpload(slavesrc="annualreport2016and17.pdf",
                         masterdest="~/buildmaster/public_html/annualreport2016and17.pdf",
                         url="http://128.39.36.155:8010/annualreport2016and17.pdf"))

    return f

def mk_cbcbeat_factory(branch='default', install_prefix=None, **kwargs):
    python = kwargs.get("python", os.environ.get("PYTHON", "python"))

    f = factory.BuildFactory()

    # Checkout
    f.addStep(steps.Mercurial(repourl="https://bitbucket.org/meg/cbcbeat/",
                              mode='full'))

    # Build (not really needed)
    f.addStep(PythonBuild(command=[python, "setup.py", "build"]))

    # Install
    f.addStep(PythonInstall(command=[python, "setup.py", "install",
                                     "--prefix=" + install_prefix]))

    # Test
    workdir = "build/test/unit"
    test_cmd = ["py.test", "-m", "'not disabled'", "-v"]
    f.addStep(Test(command=test_cmd, workdir=workdir,
                   timeout=30*60))

    return f

def mk_opentidalfarm_factory(branch="master", install_prefix=None, **kwargs):
    python = kwargs.get("python", os.environ.get("PYTHON", "python"))

    f = factory.BuildFactory()

    # Checkout
    f.addStep(steps.Git(repourl="https://github.com/funsim/OpenTidalFarm.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Build (not really needed)
    f.addStep(PythonBuild(command=[python, "setup.py", "build"]))

    # Install
    f.addStep(PythonInstall(command=[python, "setup.py", "install",
                                     "--prefix=" + install_prefix]))

    # Test
    workdir = "build/tests"
    test_cmd = ["py.test", "-v"]
    f.addStep(Test(command=test_cmd, workdir=workdir,
                   timeout=30*60))

    return f

def mk_cbcflow_factory(branch="master", install_prefix=None, **kwargs):
    f = factory.BuildFactory()

    # Checkout
    f.addStep(steps.Git(repourl="https://bitbucket.org/simula_cbc/cbcflow.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Build (not really needed)
    f.addStep(PythonBuild(command=["python", "setup.py", "build"]))

    # Install
    f.addStep(PythonInstall(command=["python", "setup.py", "install",
                                     "--prefix=" + install_prefix]))

    # Download meshes
    f.addStep(ShellCommand(command=["./scripts/cbcflow-get-data"],
                           description="download meshes",
                           descriptionDone="download meshes",
                           name="download meshes",
                           haltOnFailure=False, flunkOnFailure=False))

    # Run unit tests
    f.addStep(Test(command=["py.test", "unit"],
                   description="test unit",
                   descriptionDone="test unit",
                   workdir="build/test", timeout=30*60))

##    # Download regression data
##    f.addStep(ShellCommand(command=["./scripts/download.py"],
##                           description="download regression data",
##                           descriptionDone="download regression data",
##                           name="download regression data",
##                           haltOnFailure=False, flunkOnFailure=False,
##                           workdir="build/test"))
##
##    # Run regression tests
##    f.addStep(Test(command=["py.test", "regression"],
##                   description="test regression",
##                   descriptionDone="test regression",
##                   workdir="build/test", timeout=30*60))

    # Run demos
    f.addStep(Test(command=["./cbcflow_demos.py", "--run"],
                   description="test demo",
                   descriptionDone="test demo",
                   workdir="build/demo", timeout=30*60))

    return f

def mk_cbcpost_factory(branch="master", install_prefix=None, **kwargs):
    f = factory.BuildFactory()

    # Checkout
    f.addStep(steps.Git(repourl="https://bitbucket.org/simula_cbc/cbcpost.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Build (not really needed)
    f.addStep(PythonBuild(command=["python", "setup.py", "build"]))

    # Install
    f.addStep(PythonInstall(command=["python", "setup.py", "install",
                                     "--prefix=" + install_prefix]))

    # Run unit tests
    f.addStep(Test(command=["py.test", "--all"],
                   description="test unit",
                   descriptionDone="test unit",
                   workdir="build/test", timeout=30*60))

    # Run unit tests in parallel
    f.addStep(Test(command=["mpirun", "-n", "3", "py.test", "--all"],
                   description="test unit (parallel)",
                   descriptionDone="test unit (parallel)",
                   workdir="build/test", timeout=30*60))

    return f


#### single-slave builders

for sl in get_slaves(run_single=True).values():

    for branch in sl.branches:
        prefix = sl.install_prefix + "/" + branch
        builder_env = sl.env.copy()
        builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)
        builder_env['INSTANT_CACHE_DIR'] = prefix + "/instant-cache"
        builder_env['INSTANT_ERROR_DIR'] = prefix + "/instant-error"

        if sl.cbcflow and branch == "master":
            project = "cbcflow"
            f = mk_cbcflow_factory(branch=branch, install_prefix=prefix)
            builders.append({
                'name': '%s-%s-%s' % (project, branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, branch),
                'env': builder_env,
                })

        if sl.cbcpost and branch == "master":
            project = "cbcpost"
            f = mk_cbcpost_factory(branch=branch, install_prefix=prefix)
            builders.append({
                'name': '%s-%s-%s' % (project, branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, branch),
                'env': builder_env,
                })

        if sl.opentidalfarm and branch == "master":
            project = "opentidalfarm"
            f = mk_opentidalfarm_factory(branch=branch, install_prefix=prefix)
            builders.append({
                'name': '%s-%s-%s' % (project, branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, branch),
                'env': builder_env,
                })

        if sl.cbcbeat:
            project = "cbcbeat"
            _branch = "default"
            f = mk_cbcbeat_factory(branch=_branch, install_prefix=prefix)
            builders.append({
                'name': '%s-%s-%s' % (project, _branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, _branch),
                'env': builder_env,
                })

        if sl.annualreport:
            project = "annualreport"
            _branch = "default"
            f = mk_annualreport_factory(branch=_branch, install_prefix=prefix)
            builders.append({
                'name': '%s-%s-%s' % (project, _branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, _branch),
                'env': builder_env,
                })
