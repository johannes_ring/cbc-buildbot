import sys, os
from buildbot.buildslave import BuildSlave

class MySlaveBase(object):
    # Where should the software be installed?
    install_prefix = "/home/cbc-buildbot/cbcbbot"

    # Define evinronment variable for slave
    env = {}

    # Keyword arguments to be sent to builders
    kwargs = {}

    # true if this box is cbc.simula.no, and can build docs, etc.
    cbc_simula_no = False

    # Python version on this slave
    pyver = "2.7"

    # true if this box can generate documentation.
    documentation = False

    # true if this box can run benchmarks.
    benchmark = False

    # true if this box can generate coverage reports
    coverage = False

    # true if this box should generate and upload snapshots
    snapshot = False

    # true if this box should use a 'simple' factory
    use_simple = True

    # true if this box should use a 'full' factory
    use_full = True

    # true if this slave should have a single-slave builder of its own
    run_single = True

    # operating system
    os = None

    # build and run tests for cbcflow
    cbcflow = False

    # build and run tests for cbcpost
    cbcpost = False

    # build and run tests for OpenTidalFarm
    opentidalfarm = False

    # build and run tests for OpenTidalFarm
    cbcbeat = False

    # generate pdf for annual report
    annualreport = False

    # branches to build
    branches = ["master",]

    def extract_attrs(self, name, **kwargs):
        self.slavename = name
        remaining = {}
        for k in kwargs:
            if hasattr(self, k):
                setattr(self, k, kwargs[k])
            else:
                remaining[k] = kwargs[k]
        return remaining

    def get_pass(self, name):
        # get the password based on the name
        path = os.path.join(os.path.dirname(__file__), "%s.pass" % name)
        pw = open(path).read().strip()
        return pw

    def get_ec2_creds(self, name):
        path = os.path.join(os.path.dirname(__file__), "%s.ec2" % name)
        return open(path).read().strip().split(" ")

class MySlave(MySlaveBase, BuildSlave):
    def __init__(self, name, **kwargs):
        password = self.get_pass(name)
        kwargs = self.extract_attrs(name, **kwargs)
        BuildSlave.__init__(self, name, password, **kwargs)

slaves = [

    MySlave('ubuntu-amd64',  # ssh cbc-buildbot@192.168.203.80
            max_builds=2,
            cbcbeat=True,
            cbcflow=True,
            cbcpost=True,
            opentidalfarm=True,
            ),

    MySlave('ubuntu-cbc-misc',  # ssh cbc-buildbot@128.39.36.156
            max_builds=2,
            cbcbeat=True,
            cbcflow=True,
            cbcpost=True,
            install_prefix="/home/buildbot/cbcbbot",
            ),

    MySlave('ubuntu-cbc-docs',  # ssh cbc-buildbot@128.39.36.155
            max_builds=2,
            annualreport=True,
            install_prefix="/home/johannr/cbcbbot",
            ),
]

# these are slaves that haven't been up and from whose owners I have
# not heard in a while
retired_slaves = [
]

def get_slaves(db=None, *args, **kwargs):
    rv = {}
    for arg in args:
        rv.update(arg)
    for sl in slaves:
        if db and db not in sl.databases:
            continue
        for k in kwargs:
            if getattr(sl, k) != kwargs[k]:
                break
        else:
            rv[sl.slavename] = sl
    return rv

def names(slavedict):
    return slavedict.keys()
