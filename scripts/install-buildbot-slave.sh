#!/bin/bash

: ${INSTALL_PREFIX:=$PWD/local}
: ${VERSION=0.8.12}

pyver=$(python -c "import platform;print platform.python_version()[0:3]")
sp_dir=${INSTALL_PREFIX}/lib/python${pyver}/site-packages
mkdir -p ${sp_dir}

cat > buildbot.conf <<EOF
export PATH=${INSTALL_PREFIX}/bin\${PATH:+:\$PATH}
export PYTHONPATH=${sp_dir}\${PYTHONPATH:+:\$PYTHONPATH}
EOF

source buildbot.conf

TMPDIR=${PWD}/tmp
mkdir -p ${TMPDIR}
cd ${TMPDIR}

# Install buildbot slave requirements
easy_install --prefix=${INSTALL_PREFIX} twisted==12.1.0

# Install buildbot slave
wget https://pypi.python.org/packages/source/b/buildbot-slave/buildbot-slave-${VERSION}.tar.gz
tar -zxf buildbot-slave-${VERSION}.tar.gz
cd buildbot-slave-${VERSION}
python setup.py build
python setup.py install --prefix=${INSTALL_PREFIX}
cd ../..

echo
echo "Run the following command to update your environment variables:"
echo
echo "  source buildbot.conf"
echo
